# character_profile

The list of anime/manga characters' profiles I extracted from wikipedia. It includes 3 columns: characters' names, profile, the title of anime/manga. As of now, it has approximately 26,000 rows.


# キャラクタープロファイルデータ

アニメ/漫画のキャラクターのプロファイルのリスト。データはwikipediaより取得。キャラクター名、プロファイル、作品名の3つのカラムからなる。現在のところ約26000行ある。